//
// Created by neboula on 2019-12-30.
//

#include <SDL_mouse.h>
#include "MouseManager.h"

void MouseManager::update() {
    SDL_GetMouseState(&_x, &_y);
}

int MouseManager::getX() {
    return _x;
}

int MouseManager::getY() {
    return _y;
}
