//
// Created by neboula on 10/03/19.
//

#include <random>
#include <iostream>
#include <fstream>
#include "MapManager.h"
#include "Noise.h"
#include "Logging.h"
#include "NBML.h"

MapManager::MapManager() = default;

MapManager::MapManager(const int seed, const std::string& name, const uint length, const uint height) {
    _seed = seed;
    _name = name;

    std::string filepath = "saves/" + _name + ".txt";

    std::ifstream saveFile(filepath);

    if (saveFile.fail()) {
        perror(filepath.c_str());
    }

    if (saveFile.good()) {
        std::string contents;
        contents.assign((std::istreambuf_iterator<char>(saveFile)),
                                (std::istreambuf_iterator<char>()));

        std::cout << contents;

        _map = NBML::NBMLToMap(contents);

        _length = _map.size();
        _height = _map[0].size();
    } else {
        _length = length;
        _height = height;
        _map = std::vector<std::vector<uint> >(_length);

        for (uint x = 0; x < _length; x++) {
            _map[x].resize(_height);
        }

        _seedArray.resize(_length);
        _noiseArray.resize(_length);
        _treeSeedArray.resize(_length);
        _treeNoiseArray.resize(_length);

        generate();

        std::ofstream saveFileWriter(filepath.c_str());
        saveFileWriter << textifyMap();
        saveFileWriter.close();
    }
}

void MapManager::generate() {
    std::mt19937 randomEngine(_seed);
    std::uniform_real_distribution<float> range(0, 1);

    for (uint i = 0; i < _length; i++) {
        _seedArray[i] = range(randomEngine);
        _treeSeedArray[i] = range(randomEngine);
    }

    Noise::genNoise1D(_length, &_seedArray, 4, &_noiseArray);
    Noise::genNoise1D(_length, &_treeSeedArray, 4, &_treeNoiseArray);

    /*
     * 0 for air, 1 for stone, 2 for earth. We'll see about the rest.
     * Should probably write this elsewhere.
     */

    std::cout << "Generating terrain ...\n";
    for (uint x = 0; x < _length; x++) {
        const auto GROUND_HEIGHT = static_cast<uint>(std::roundf((_noiseArray[x] * static_cast<float>(_height)) / 2));
        for (uint y = 0; y < _height; y++) {
            if (y < GROUND_HEIGHT - 4) {
                _map[x][y] = 0;
            } else if (y < GROUND_HEIGHT - 3) {
                _map[x][y] = 2;
            } else if (y < GROUND_HEIGHT) {
                _map[x][y] = 5;
            } else {
                if (range(randomEngine) < 0.98f) {
                    _map[x][y] = 1;
                } else {
                    _map[x][y] = 3;
                }
            }
        }
    }

    std::cout << "Adding trees ...\n";

    std::uniform_int_distribution<u_char> treeHeightRange(3 ,7);
    std::uniform_int_distribution<u_char> plantTreeRange(0, 1);

    for (uint x = 0; x < _length; x++) {
        if (_treeNoiseArray[x] > 0.6f) {
            u_char plantTree = plantTreeRange(randomEngine);
            if (plantTree) {
                for (uint y = 0; y < _height; y++) {
                    if (_map[x][y] != 0) {
                        std::cout << "Planting tree ... \n";

                        uint treeHeight = treeHeightRange(randomEngine);


                        for (uint i = 1; i < treeHeight; i++) {
                            setTile(x, y - i, 4);
                        }

                        // In order to prevent trees from growing directly next to
                        // each other, increment the counter when tree was planted
                        x++;
                        break;
                    }
                }
            }
        }
    }
}

void MapManager::debugPrint() const {
    for (uint y = 0; y < _length; y++) {
        for (uint x = 0; x < _height; x++) {
            std::cout << _map[x][y] << ' ';
        }
        std::cout << '\n';
    }
}

uint MapManager::getTile(uint x, uint y) const {
    if (x > _length || y > _height) {
        std::string error = "Attempt to get tile outside of map (x: " + std::to_string(x) + ", y: " + std::to_string(y) + ")";
        Logging::warning(error);
        return 9;
    }
    return _map[x][y];
}

std::string MapManager::getName() const {
    return _name;
}

uint MapManager::getWidth() const {
    return _length;
}

uint MapManager::getHeight() const {
    return _height;
}

bool MapManager::isInsideMap(long x, long y) const {
    if (x < 0) {
        return false;
    }

    if (y < 0) {
        return false;
    }

    if (y > _height) {
        return false;
    }

    return x <= _length;

}

void MapManager::setTile(long x, long y, uint type) {
    if (isInsideMap(x, y)) {
        _map[x][y] = type;
    }
}

float MapManager::getGravity() const {
    return _gravitationalConstant;
}

std::string MapManager::textifyMap() {
    NBML data;

    data.addLabel("HEAD");
    data.addValue("width", std::to_string(_length));
    data.addValue("height", std::to_string(_height));
    data.addLabel("MAP");
    for (auto& column : _map) {
        for (auto& element : column) {
            data.addParticle(std::to_string(element));
        }
        data.addParticle("split");
    }
    data.addLabel("END");

    return data.getData();
}

MapManager::~MapManager() = default;
