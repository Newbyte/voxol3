//
// Created by neboula on 10/03/19.
//

#include "Client.h"
#include "Player.h"
#include "TextureManager.h"
#include "Logging.h"
#include <SDL2/SDL_net.h>
#include <iostream>
#include <chrono>
#include <thread>

Client::Client(std::string worldName) {
    _window = SDL_CreateWindow(
            "Voxol",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            720,
            720,
            SDL_WINDOW_RESIZABLE
    );

    _map = MapManager(static_cast<int>(time(nullptr)), worldName, 512, 256);

    _map.debugPrint();


    _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);
    _textureManager = TextureManager(_renderer);
    _painter = TilePainter(_renderer, &_textureManager);
    _player = new Player(_renderer, _textureManager.loadTexture("character.png"), &_map, 0.5 + (_map.getWidth() / 2.0f), _map.getHeight() / 2.0f);
    _camera = Camera(_player, _window);
    _entityManager = EntityManager(_renderer, &_camera);

    _entityManager.addEntity(_player);
    _entityManager.addEntity(new Player(_renderer, _textureManager.loadTexture("character.png"), &_map, 5.0 + (_map.getWidth() / 2.0), _map.getHeight() / 2.0f));

    _player->setX(_map.getWidth() / 2.0f);
    _player->setY(_map.getHeight() / 2.0f);

    const unsigned char FPS = 60;
    const unsigned char FRAME_DELAY = 1000 / FPS;

    // Set the player's Y-coordinate to ground height
    for (uint y = 0; y < _map.getHeight(); y++) {
        if (_map.getTile(_player->getX(), y) != 0) {
            _player->setY(y - _player->getHeight());
            break;
        }
    }

    Uint32 frameStart;
    uint frameTime;

    std::thread inputThread(handleEvents, &_inputManager, &_gameState);

    while (_gameState == GameState::PLAYING) {
        frameStart = SDL_GetTicks();

        update();
        draw();

        frameTime = SDL_GetTicks() - frameStart;

        // If we render more than 60 frames per second, delay (wait)
        if (FRAME_DELAY > frameTime) {
            SDL_Delay(FRAME_DELAY - frameTime);
        }
    }

    inputThread.join();
}

void Client::draw() {
    SDL_RenderClear(_renderer);
    std::string textureName;

    for (uint x = 0; x < _camera.getNumTilesWidth(); x++) {
        for (uint y = 0; y < _camera.getNumTilesHeight(); y++) {
            const double currentX = x + _camera.getX();
            const double currentY = y + _camera.getY();

            switch (_map.getTile(currentX, currentY)) {
                case 0:
                    textureName = "sky";
                    break;
                case 1:
                    textureName = "stone";
                    break;
                case 2:
                    textureName = "earth-grass";
                    break;
                case 3:
                    textureName = "blue-stone";
                    break;
                case 4:
                    textureName = "log";
                    break;
                case 5:
                    textureName = "earth";
                    break;
                case 9: // Intentional fallthrough. 9 means invalid tile. Arbitrary number? Yes.
                default:
                    textureName = "missing-texture";
                    break;
            }
            textureName.append(".png");

            _painter.paint(x * _camera.getScale() - _camera.getXOffset() * _camera.getScale(),
                           y * _camera.getScale() - _camera.getYOffset() * _camera.getScale(),
                           _camera.getScale(),
                           textureName.c_str()
            );
        }
    }

    _entityManager.draw();

    SDL_RenderPresent(_renderer);
}

void Client::handleEvents(InputManager* inputManager, GameState* gameState) {
    while ((*gameState) == GameState::PLAYING) {
        using namespace std::chrono_literals;

        inputManager->checkInput();
        std::this_thread::sleep_for(10ms);
    }
}

void Client::update() {
    const std::set<SDL_Keycode>* pressedKeys = _inputManager.getPressedKeys();

    std::set<SDL_Keycode>::iterator it;
    for (it = pressedKeys->begin(); it != pressedKeys->end(); it++) {
        switch (*it) {
            case SDLK_s:
                _player->incrementY(0.25);
                break;
            case SDLK_w:
                _player->incrementY(-0.25);
                break;
            case SDLK_a:
                _player->incrementX(-0.25);
                break;
            case SDLK_d:
                _player->incrementX(0.25);
                break;
            case SDLK_SPACE:
            case SDLK_KP_SPACE:
                _player->jump();
                break;
            case SDLK_PLUS:
            case SDLK_KP_PLUS:
                _camera.incrementScale(1);
                break;
            case SDLK_MINUS:
            case SDLK_KP_MINUS:
                _camera.incrementScale(-1);
                break;
            default:
                break;
        }
    }

    if (_inputManager.shouldQuit()) {
        _gameState = GameState::EXIT;
    }

    _entityManager.updateEntities();

    _camera.update();
}

Client::~Client() {
    SDL_DestroyWindow(_window);
    SDL_DestroyRenderer(_renderer);
    SDLNet_Quit();
    SDL_Quit();
}

Client::Client(const std::string& host, u_int16_t port) {
    IPaddress ip;
    constexpr int mapSize = 100000;

    SDLNet_ResolveHost(&ip, host.c_str(), port);

    TCPsocket client = SDLNet_TCP_Open(&ip);

    char map[mapSize];

    SDLNet_TCP_Recv(client, map, mapSize);

    std::cout << map;

    _window = SDL_CreateWindow(
            "Voxol Multiplayer",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            720,
            720,
            SDL_WINDOW_RESIZABLE
            );

    _renderer = SDL_CreateRenderer(_window, -1, 0);
    _textureManager = TextureManager(_renderer);
    _painter = TilePainter(_renderer, &_textureManager);
    //_player = new Player(_renderer, _textureManager.loadTexture("character.png"), 0, 0);


    SDLNet_TCP_Close(client);
}
