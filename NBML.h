//
// Created by neboula on 2019-04-30.
//

#ifndef VOXOL3_NBML_H
#define VOXOL3_NBML_H


#include <string>
#include <vector>

class NBML {
public:
    void addLabel(std::string text);
    void addParticle(std::string text);
    void addValue(std::string name, std::string data);
    std::string getData() const;
    static std::vector<std::vector<unsigned int> > NBMLToMap(const std::string& NBMLString);
private:
    std::string _data;
};


#endif //VOXOL3_NBML_H
