//
// Created by neboula on 2019-03-13.
//

#include "TilePainter.h"

TilePainter::TilePainter(SDL_Renderer* renderer, TextureManager* textureManager) {
    _renderer = renderer;
    _textureManager = textureManager;
    _srcRect.x = 0;
    _srcRect.y = 0;
    _srcRect.w = 16;
    _srcRect.h = 16;

    _destRect.x = 0;
    _destRect.y = 0;
    _destRect.w = 32;
    _destRect.h = 32;
}

TilePainter::TilePainter() = default;

void TilePainter::paint(const int& x, const int& y, const int& scale, const char* filename) {
    SDL_Texture* texture = _textureManager->loadTexture(filename);

    _destRect.x = x;
    _destRect.y = y;
    _destRect.w = scale;
    _destRect.h = scale;

    SDL_RenderCopy(_renderer, texture, &_srcRect, &_destRect);
}
