//
// Created by neboula on 2019-04-05.
//

#include "Logging.h"
#include <iostream>
#include <SDL2/SDL_quit.h>

void Logging::fatalError(const std::string& err, int code) {
    std::cerr << err << '\n';
    exit(code);
}

void Logging::fatalSDLError(const std::string& err, int code) {
    std::fprintf(stderr, err.c_str(), SDL_GetError());
    std::cout << '\n';

    exit(code);
}

void Logging::warning(const std::string &warn) {
    std::cerr << "WARNING: " << warn << '\n';
}
