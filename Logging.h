//
// Created by neboula on 2019-04-05.
//

#ifndef VOXOL3_ERRORS_H
#define VOXOL3_ERRORS_H

#include <string>

class Logging {
public:
    /// Function for crashing the program with a specified error message
    /// and code. Writes message to stderr.
    /// \param err
    /// \param code
    static void fatalError(const std::string& err, int code = 1);
    /// Function for crashing the program with a specified error message
    /// and one from SDL2, plus a specified error code.
    /// Writes message to stderr.
    /// \param err The error to print. One \%s, no more, no less, is required in the string.
    /// It will be replaced by an appropriate SDL2 error.
    /// \param code Exit code to use.
    static void fatalSDLError(const std::string& err, int code = -1);
    /// Prints out an error to STDERR without crashing the game
    /// \param warn String to print as warning
    static void warning(const std::string& warn);
};

#endif //VOXOL3_ERRORS_H
