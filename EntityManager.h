//
// Created by neboula on 2019-04-26.
//

#ifndef VOXOL3_ENTITYMANAGER_H
#define VOXOL3_ENTITYMANAGER_H

#include <vector>
#include <SDL2/SDL_render.h>
#include "Entity.h"
#include "Camera.h"


class EntityManager {
public:
    EntityManager(const SDL_Renderer* renderer, const Camera* camera);
    EntityManager();
    ~EntityManager();
    void addEntity(Entity* entity);
    void updateEntities();
    void draw();
private:
    /// Check whether an entity is within the camera's current view.
    /// \param entity Entity to check.
    /// \return True if entity is within view, false if not.
    bool entityWithinView(Entity& entity);
    const SDL_Renderer* _renderer = nullptr;
    const Camera* _camera = nullptr;
    std::vector<Entity*> _entities = std::vector<Entity*>();
};


#endif //VOXOL3_ENTITYMANAGER_H
