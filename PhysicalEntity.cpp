//
// Created by neboula on 2019-04-28.
//

#include "PhysicalEntity.h"

PhysicalEntity::PhysicalEntity(SDL_Renderer *renderer, SDL_Texture *texture, MapManager* map, float x, float y) : Entity(renderer,
                                                                                                        texture, x,
                                                                                                        y) {
    _map = map;
}

void PhysicalEntity::update() {
    _onGround = _map->getTile(static_cast<unsigned int>(std::roundf(_x)), std::floor(_y) + _height) != 0;

    _y += _resultingForce;

    if (_onGround) {
        _resultingForce = 0;
    } else if (_resultingForce < 0.1) {
        _resultingForce += _map->getGravity();
    }
}
