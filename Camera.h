//
// Created by neboula on 2019-04-07.
//

#ifndef VOXOL3_CAMERA_H
#define VOXOL3_CAMERA_H

#include "Entity.h"
#include <SDL2/SDL_render.h>

class Camera {
public:
    /// Constructs a new camera and calls Camera::update().
    /// \param player A player object to position the camera relative to.
    /// \param window
    Camera(Entity* player, SDL_Window* window);
    /// Constructs a new camera. Do not use a camera constructed via this,
    /// as not all member variables are properly initialised with it.
    Camera();
    /// Updates the camera's coordinates and other member variables,
    /// like width in tiles.
    void update();
    /// Function to increment the _scale member variable.
    /// Can also be used for decrementing.
    /// \param term Amount to add / subtract.
    void incrementScale(int term);
    float getX() const;
    float getY() const;
    /// Get decimal part of _x.
    /// \return Decimal part of _x.
    float getXOffset() const;
    /// Get decimal part of _y.
    /// \return Decimal part of _y.
    float getYOffset() const;
    /// Get the width of window.
    /// \return Width of window in pixels.
    int getWidth() const;
    /// Get height of window.
    /// \return Height of window in pixels.
    int getHeight() const;
    /// Get the number of tiles that should be drawn to the screen vertically.
    /// \return Unsigned integer representing the amount of tiles that should
    /// be drawn onto the screen vertically to fill it.
    unsigned int getNumTilesWidth() const;
    /// Get the number of tiles that should be drawn to the screen horizontally.
    /// \return Unsigned integer representing the amount of tiles that should
    /// be drawn onto the screen horizontally to fill it.
    unsigned int getNumTilesHeight() const;
    /// Getter function for the _scale variable.
    uint getScale() const;
private:
    /// Word-space X-coordinate for the camera.
    float _x = 0.0f;
    /// Word-space Y-coordinate for the camera.
    float _y = 0.0f;
    /// Width of the viewport, in pixels.
    int _width = 512;
    /// Height of the viewport, in pixels.
    int _height = 512;
    /// Scale determines how big each block is.
    uint _scale = 32;
    uint _numTilesWidth = 0;
    uint _numTilesHeight = 0;
    SDL_Window* _window = nullptr;
    Entity* _player = nullptr;
};


#endif //VOXOL3_CAMERA_H
