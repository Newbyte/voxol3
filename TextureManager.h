//
// Created by neboula on 10/03/19.
//

#ifndef VOXOL3_TEXTUREMANAGER_H
#define VOXOL3_TEXTUREMANAGER_H

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <unordered_map>

class TextureManager {
public:
    explicit TextureManager(SDL_Renderer* renderer);
    TextureManager();
    /// Function that loads a specified texture. If no texture with the specified filename is found, the game will crash
    /// \param filename Name of texture to load. Must include file extension if there is one (for example, tree.png)
    /// \param renderer Pointer to the renderer to used to create the texture. Should be the same one as used everywhere else in the code
    /// \return Returns a pointer to an SDL_Texture
    SDL_Texture* loadTexture(const std::string& filename);
private:
    /// Contains the textures currently loaded in memory. Uses path to texture/filename as key and texture itself as value
    std::unordered_map<std::string, SDL_Texture*> _loadedTextures;
    SDL_Renderer* _renderer = nullptr;
};


#endif //VOXOL3_TEXTUREMANAGER_H
