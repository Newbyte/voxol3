//
// Created by neboula on 2019-03-14.
//

#ifndef VOXOL3_INPUTMANAGER_H
#define VOXOL3_INPUTMANAGER_H


#include <set>
#include <mutex>

class InputManager {
public:
    const bool shouldQuit();
    void checkInput();
    const std::set<SDL_Keycode>* getPressedKeys();
private:
    std::set<SDL_Keycode> _pressedKeys;
    std::mutex _mutex;
    bool _shouldExit = false;
};


#endif //VOXOL3_INPUTMANAGER_H
