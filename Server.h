//
// Created by neboula on 10/03/19.
//

#ifndef VOXOL3_SERVER_H
#define VOXOL3_SERVER_H

#include "MapManager.h"

class Server {
public:
    Server(std::string worldName, uint16_t port);
private:
    MapManager _mapManager;
};

#endif //VOXOL3_SERVER_H
