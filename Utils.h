//
// Created by neboula on 2019-05-01.
//

#ifndef VOXOL3_UTILS_H
#define VOXOL3_UTILS_H


#include <string>
#include <vector>

class Utils {
public:
    static std::vector<std::string> splitString(const std::string& string, char delimiter);
};


#endif //VOXOL3_UTILS_H
