//
// Created by neboula on 2019-12-30.
//

#ifndef VOXOL3_MOUSEMANAGER_H
#define VOXOL3_MOUSEMANAGER_H

class MouseManager {
public:
    MouseManager();
    void update();
    int getX();
    int getY();
private:
    int _x = 0;
    int _y = 0;
};


#endif //VOXOL3_MOUSEMANAGER_H
