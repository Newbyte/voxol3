//
// Created by neboula on 10/03/19.
//

#ifndef VOXOL3_NOISE_H
#define VOXOL3_NOISE_H

#include <vector>

class Noise {
public:
    /// Generates 1-dimensional noise based on a 1-dimensional seed
    /// \param count
    /// \param seed Vector with random numbers to be used as seed
    /// \param octaves Number of octaves to generate
    /// \param output Vector to contain the resulting noise
    static void genNoise1D(unsigned int count, const std::vector<float> *seed, unsigned int octaves, std::vector<float> *output);
};

#endif //VOXOL3_NOISE_H
