## Build instructions

This assumes that you are using Linux, or other UNIX-like operating system with support for CMake and makefiles.
- Clone this git repo
- Enter its root directory
- Create a new directory called "build"
- Change directory to build
- Run `cmake ..`, followed by `make`
