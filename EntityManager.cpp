//
// Created by neboula on 2019-04-26.
//

#include "EntityManager.h"

EntityManager::EntityManager(const SDL_Renderer *renderer, const Camera* camera) {
    _renderer = renderer;
    _camera = camera;
}

void EntityManager::addEntity(Entity* entity) {
    _entities.push_back(entity);
}

void EntityManager::draw() {
    for (auto& entity : _entities) {
        if (entityWithinView(*entity)) {
            entity->draw((entity->getX() - _camera->getX()) * _camera->getScale(),
                        (entity->getY() - _camera->getY()) * _camera->getScale(),
                        _camera->getScale());
        }
    }
}

bool EntityManager::entityWithinView(Entity& entity) {
    return !(entity.getX() > _camera->getX() + _camera->getNumTilesWidth() ||
             entity.getX() < _camera->getX() ||
             entity.getY() > _camera->getY() + _camera->getNumTilesHeight() ||
             entity.getY() < _camera->getY());
}

void EntityManager::updateEntities() {
    for (auto& entity : _entities) {
        entity->update();
    }
}

EntityManager::~EntityManager() {
    for (auto& entity : _entities) {
        delete entity;
    }
}

EntityManager::EntityManager() = default;
