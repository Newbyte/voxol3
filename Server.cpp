//
// Created by neboula on 10/03/19.
//

#include <SDL_net.h>
#include "Server.h"

Server::Server(std::string worldName, uint16_t port) {
    IPaddress ip;

    _mapManager = MapManager(static_cast<int>(time(nullptr)), worldName, 512, 256);
    std::string textifiedMap = _mapManager.textifyMap();

    bool serverRunning = true;

    SDLNet_ResolveHost(&ip, nullptr, port);

    TCPsocket clientSock;
    TCPsocket serverSock = SDLNet_TCP_Open(&ip);

    while (serverRunning) {
        clientSock = SDLNet_TCP_Accept(serverSock);

        if (clientSock) {
            SDLNet_TCP_Send(clientSock, textifiedMap.c_str(), textifiedMap.size() + 1);
        }
    }
}
