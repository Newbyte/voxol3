//
// Created by neboula on 2019-04-28.
//

#ifndef VOXOL3_PHYSICALENTITY_H
#define VOXOL3_PHYSICALENTITY_H


#include "Entity.h"
#include "MapManager.h"

class PhysicalEntity : public Entity {
public:
    PhysicalEntity(SDL_Renderer *renderer, SDL_Texture *texture, MapManager* map, float x, float y);
    void update() override;
protected:
    MapManager* _map = nullptr;
    bool _onGround = true;
    float _resultingForce = 0.1;
};


#endif //VOXOL3_PHYSICALENTITY_H
