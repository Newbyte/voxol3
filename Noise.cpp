//
// Created by neboula on 10/03/19.
//

#include "Noise.h"

void Noise::genNoise1D(unsigned int count, const std::vector<float>* seed, unsigned int octaves, std::vector<float>* output) {
    for (unsigned int x = 0; x < count; x++) {
        float noise = 0.0f;
        float scale = 1.0f;
        float scaleAcc = 0.0f;

        for (unsigned int o = 0; o < octaves; o++) {
            int pitch = count >> o;
            int sample1 = (x / pitch) * pitch;
            int sample2 = (sample1 + pitch) % count;

            float blend = (float) (x - sample1) / (float) pitch;
            float sample = (1.0f - blend) * (*seed)[sample1] + blend * (*seed)[sample2];

            noise += sample * scale;
            scaleAcc += scale;
            scale = scale / 2.0f;
        }

        (*output)[x] = noise / scaleAcc;
    }
}
