//
// Created by neboula on 10/03/19.
//

#ifndef VOXOL3_MAPMANAGER_H
#define VOXOL3_MAPMANAGER_H

#include <string>
#include <vector>

class MapManager {
public:
    /// Constructs a new MapManager and generates a map along with it via MapManager::generate()
    /// \param seed A numeric seed to used for generating the map. Using the same seed twice will generate the same map both times, unless the world generator has been changed between them
    /// \param name The name of the map to generate. Currently unused, but may be used should saving be implemented
    /// \param length The length (in tiles), or width, of the map to generate
    /// \param height The height (in tiles) of the map to generate
    MapManager(int seed, const std::string& name, uint length, uint height);
    /// Default constructor for MapManager. Does not generate a map
    MapManager();
    /// Destructing the MapManager does not delete anything on heap by itself, however the contents of the vectors does get deleted as they go out of scope
    ~MapManager();
    /// Generate a new map using the currently specified parameters, usually the ones set in the constructor
    void generate();
    /// Prints the map to STDOUT using numbers corresponding to the type of tile. Should only be used for debugging purposes, as name suggests
    void debugPrint() const;
    /// Get the tile of the specified coordinates. Should the coordinates point to a tile outside of the map a warning will be generated
    /// \param x X-coordinate to pick the tile from
    /// \param y Y-coordinate to pick the tile from
    /// \return Returns an unsigned integer. If said integer is 9 the coordinates were outside of the map
    uint getTile(uint x, uint y) const;
    /// Gets the width of the map, in tiles
    uint getWidth() const;
    /// Gets the height of the map, in tiles
    uint getHeight() const;
    float getGravity() const;
    /// Gets the name of the current map. Currently not used for anything, but could be used should saving be implemented
    std::string getName() const;
    std::string textifyMap();
    void setTile(long x, long y, uint type);
    bool isInsideMap(long x, long y) const;
private:
    int _seed = 0;
    uint _height = 0;
    uint _length = 0;
    float _gravitationalConstant = 0.00982;
    std::string _name;
    std::vector<float> _seedArray;
    std::vector<float> _noiseArray;
    std::vector<float> _treeSeedArray;
    std::vector<float> _treeNoiseArray;
    std::vector<std::vector<uint> > _map;
};


#endif //VOXOL3_MAPMANAGER_H
