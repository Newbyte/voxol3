//
// Created by neboula on 2019-04-27.
//

#ifndef VOXOL3_PLAYER_H
#define VOXOL3_PLAYER_H


#include "PhysicalEntity.h"

class Player : public PhysicalEntity {
public:
    Player(SDL_Renderer *renderer, SDL_Texture *texture, MapManager* map, float x, float y);
    void update() override;
    void jump();
};

#endif //VOXOL3_PLAYER_H
