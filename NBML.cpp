//
// Created by neboula on 2019-04-30.
//

#include "NBML.h"
#include "Utils.h"

void NBML::addLabel(std::string text) {
    _data.append("!" + text + ";");
}

void NBML::addValue(std::string name, std::string data) {
    _data.append("$" + name + ":" + data + ";");
}

void NBML::addParticle(std::string text) {
    _data.append(text + ";");
}

std::string NBML::getData() const {
    return _data;
}

std::vector<std::vector<unsigned int> > NBML::NBMLToMap(const std::string& NBMLString) {
    std::vector<std::vector<unsigned int> > map;
    std::vector<std::string> valuePair;
    std::vector<std::string> nameAndValue;

    auto fragments = Utils::splitString(NBMLString, ';');

    enum class OperationModes {
        UNKNOWN,
        METADATA_GATHERING,
        MAP_WRITING
    };

    OperationModes operationMode = OperationModes::UNKNOWN;

    uint x = 0, y = 0;

    for (auto& fragment : fragments) {
        switch (fragment[0]) {
            case '!': // Path for labels
                if (fragment == "!HEAD") {
                    operationMode = OperationModes::METADATA_GATHERING;
                } else if (fragment == "!MAP") {
                    operationMode = OperationModes::MAP_WRITING;
                } else if (fragment == "!END") {
                    return map;
                }
                break;
            case '$': // Path for values
                if (operationMode == OperationModes::METADATA_GATHERING) {
                    nameAndValue = Utils::splitString(fragment, ':');
                    if (nameAndValue[0] == "$width") {
                        map.resize(std::stoi(nameAndValue[1]));
                    } else if (nameAndValue[0] == "$height") {
                        for (auto &column : map) {
                            column.resize(std::stoi(nameAndValue[1]));
                        }
                    }
                }
                break;
            default: // Path for particles
                if (operationMode == OperationModes::MAP_WRITING) {
                    if (fragment != "split") {
                        map[x][y] = std::stoi(fragment);
                        y++;
                    } else {
                        x++;
                        y = 0;
                    }
                }
                break;
        }
    }

    return map;
}
