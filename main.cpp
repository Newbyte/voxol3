#include <iostream>
#include <cstring>
#include <inttypes.h>
#include <SDL2/SDL_net.h>
#include "Client.h"
#include "Server.h"

int main(int argc, const char** args) {
    // Client is for connecting to server, standalone is for playing on your own
    enum class ProgramRole { CLIENT, SERVER, STANDALONE };

    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        std::cout << "SDL2 initialised successfully\n";
    } else {
        std::cout << "SDL2 failed to initialise\n";
        std::cout << SDL_GetError() << '\n';
        return -1;
    }

    if (SDLNet_Init() == 0) {
        std::cout << "SDL2_net initialised successfully\n";
    } else {
        std::cout << "SDL2_net failed to initialise\n";
        std::cout << SDLNet_GetError() << '\n';
        return -2;
    }

    ProgramRole role = ProgramRole::CLIENT;
    std::string worldInputString;
    std::string host;
    uint16_t port = 13135;

    if (argc > 1) {
        for (uint i = 0; i < argc; i++) {
            if (std::strstr(args[i], "server")) {
                role = ProgramRole::SERVER;
            }
        }
    } else {
        std::string roleInputString;
        std::cout << "Greetings! Would you like to be a server or a client? (client/server): ";
        std::cin >> roleInputString;

        if (roleInputString == "client") {
            role = ProgramRole::STANDALONE;
        } else if (roleInputString == "server") {
            role = ProgramRole::SERVER;
        }

        if (role == ProgramRole::STANDALONE) {
            std::string playMode;
            std::cout << "Singleplayer or multiplayer? (single/multi): ";
            std::cin >> playMode;

            if (playMode == "multi" || playMode == "multiplayer") {
                role = ProgramRole::CLIENT;
            }
        }

        if (role != ProgramRole::CLIENT) {
            std::cout << "Enter the name of the world you'd like to play on: ";
            std::cin >> worldInputString;
        }

        if (role == ProgramRole::CLIENT) {
            std::cout << "Enter the IP or hostname of the server you wish to connect to: ";
            std::cin >> host;
        }

        if (role == ProgramRole::CLIENT || role == ProgramRole::SERVER) {
            std::cout << "Choose port: ";
            std::cin >> port;
        }
    }

    if (role == ProgramRole::STANDALONE) {
        Client client(worldInputString);
    } else if (role == ProgramRole::SERVER) {
        Server server(worldInputString, port);
    } else {
        Client client(host, port);
    }

    return 0;
}