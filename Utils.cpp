//
// Created by neboula on 2019-05-01.
//

#include <sstream>
#include "Utils.h"

std::vector<std::string> Utils::splitString(const std::string& string, char delimiter) {
    std::vector<std::string> fragments;
    std::string segment;
    std::stringstream stringstream(string);

    while (std::getline(stringstream, segment, delimiter)) {
        fragments.push_back(segment);
    }

    return fragments;
}
