//
// Created by neboula on 2019-04-21.
//

#include "Entity.h"

float Entity::getX() const {
    return _x;
}

float Entity::getY() const {
    return _y;
}

Entity::Entity(SDL_Renderer* renderer, SDL_Texture* texture, float x, float y) {
    _x = x;
    _y = y;
    _width = 1;
    _height = 2;

    _srcRect.w = 16;
    _srcRect.h = 32;
    _srcRect.x = 0;
    _srcRect.y = 0;

    _destRect.w = static_cast<int>(_width);
    _destRect.h = static_cast<int>(_height);
    _destRect.x = 0;
    _destRect.y = 0;

    _renderer = renderer;
    _texture = texture;
}

void Entity::draw(unsigned int x, unsigned int y, unsigned int scale) {
    _destRect.x = x;
    _destRect.y = y;
    _destRect.w = static_cast<int>(_width * scale);
    _destRect.h = static_cast<int>(_height * scale);

    SDL_RenderCopy(_renderer, _texture, &_srcRect, &_destRect);
}

void Entity::update() {
    _x += 0.001;
}

void Entity::setX(float x) {
    _x = x;
}

void Entity::setY(float y) {
    _y = y;
}

void Entity::incrementX(float x) {
    _x += x;
}

void Entity::incrementY(float y) {
    _y += y;
}

float Entity::getHeight() const {
    return _height;
}
