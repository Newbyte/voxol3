//
// Created by neboula on 2019-03-14.
//

#include <SDL_events.h>
#include "InputManager.h"

void InputManager::checkInput() {
    std::lock_guard<std::mutex> lockGuard(_mutex);
    SDL_Event evnt;
    while (SDL_PollEvent(&evnt)) {
        switch (evnt.type) {
            case SDL_QUIT:
                _shouldExit = true;
                break;
            case SDL_KEYDOWN:
                _pressedKeys.insert(evnt.key.keysym.sym);
                break;
            case SDL_KEYUP:
                _pressedKeys.erase(evnt.key.keysym.sym);
                break;
            default:
                break;
        }
    }
}

const bool InputManager::shouldQuit() {
    std::lock_guard<std::mutex> lockGuard(_mutex);
    return _shouldExit;
}

const std::set<SDL_Keycode>* InputManager::getPressedKeys() {
    std::lock_guard<std::mutex> lockGuard(_mutex);
    return &_pressedKeys;
}
