//
// Created by neboula on 10/03/19.
//

#include "TextureManager.h"
#include "Logging.h"
#include <string>

SDL_Texture* TextureManager::loadTexture(const std::string& filename) {
    if (_loadedTextures.find(filename) != _loadedTextures.end()) {
        // Texture is already loaded, return pointer to copy in RAM
        return _loadedTextures.find(filename)->second;
    } else {
        // Texture hasn't been loaded yet, load it from file
        std::string path = "../Assets/";
        SDL_Surface* tmpSurface = IMG_Load(path.append(filename).c_str());
        SDL_Texture* texture = SDL_CreateTextureFromSurface(_renderer, tmpSurface);

        // If no texture was received, error and crash program
        if (texture == nullptr) {
            Logging::fatalSDLError("CreateTextureFromSurface failed: %s (is " + filename + " a valid texture?)");
        }

        _loadedTextures.insert(std::make_pair(filename, texture));
        SDL_FreeSurface(tmpSurface);

        return texture;
    }
}

TextureManager::TextureManager(SDL_Renderer *renderer) {
    _renderer = renderer;
}

TextureManager::TextureManager() = default;
