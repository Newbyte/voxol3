//
// Created by neboula on 2019-04-21.
//

#ifndef VOXOL3_ENTITY_H
#define VOXOL3_ENTITY_H

#include <SDL2/SDL_render.h>

class Entity {
public:
    Entity(SDL_Renderer* renderer, SDL_Texture* texture, float x, float y);
    float getX() const;
    float getY() const;
    float getHeight() const;
    void setX(float x);
    void setY(float y);
    void incrementX(float x);
    void incrementY(float y);
    void draw(unsigned int x, unsigned int y, unsigned int scale);
    virtual void update();
protected:
    // Coordinates are the same as tiles use, except that these can be decimal
    float _x;
    float _y;
    // Dimensions are relative to tile dimensions
    float _width = 1;
    float _height = 1;
    SDL_Renderer* _renderer = nullptr;
    SDL_Texture* _texture = nullptr;
    SDL_Rect _srcRect, _destRect;
};


#endif //VOXOL3_ENTITY_H
