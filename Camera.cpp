//
// Created by neboula on 2019-04-07.
//

#include "Camera.h"

void Camera::update() {
    SDL_GetWindowSize(_window, &_width, &_height);

    _numTilesWidth = std::ceil((getWidth() / getScale()) + 2);
    _numTilesHeight = std::ceil((getHeight() / getScale()) + 2);

    _x = _player->getX() - (_numTilesWidth / 2.0f) + 1;
    _y = _player->getY() - (_numTilesHeight / 2.0f) + 1;
}

float Camera::getX() const {
    return _x;
}

float Camera::getY() const {
    return _y;
}

Camera::Camera(Entity* player, SDL_Window* window) {
    _player = player;
    _window = window;

    update();
}

uint Camera::getScale() const {
    return _scale;
}

int Camera::getWidth() const {
    return _width;
}

int Camera::getHeight() const {
    return _height;
}

void Camera::incrementScale(int term) {
    _scale += term;

    // If scale goes below 1, increment it back up. Scale reaching 0 would result in division
    // by zero later in the code -- very undesirable
    if (_scale < 1) {
        _scale = 1;
    }
}

float Camera::getXOffset() const {
    float foo;
    return modf(_x, &foo);
}

float Camera::getYOffset() const {
    float foo;
    return modf(_y, &foo);
}

unsigned int Camera::getNumTilesWidth() const {
    return _numTilesWidth;
}

unsigned int Camera::getNumTilesHeight() const {
    return _numTilesHeight;
}

Camera::Camera() = default;
