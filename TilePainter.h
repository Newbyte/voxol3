//
// Created by neboula on 2019-03-13.
//

#ifndef VOXOL3_TILEPAINTER_H
#define VOXOL3_TILEPAINTER_H

#include <SDL2/SDL_render.h>
#include "TextureManager.h"

class TilePainter {
public:
    /// Constructs a new TilePainter object using the specified renderer and TextureManager
    /// \param renderer Renderer to use. Should be the same one as in the rest of the game
    /// \param textureManager TextureManager to use. Should be the same one as in the rest of the game
    TilePainter(SDL_Renderer* renderer, TextureManager* textureManager);
    /// Default constructor for TilePainter. Attempting to use a TilePainter constructed using this will result in errors
    TilePainter();
    /// Function used to draw tiles to the screen
    /// \param x X-coordinate in screen-space to draw the tile to. Starts in top-left corner
    /// \param y Y-coordinate in screen-space to draw the tile to. Starts in top-left corner
    /// \param scale Multiplier for how large the texture drawn should be. For example, 1 would mean that it takes up 1x1 pixel on the screen, 32 would mean that it takes up 32x32 pixels on the screen
    /// \param filename Filename of the texture to draw to the screen
    void paint(const int& x, const int& y, const int& scale, const char* filename);
private:
    SDL_Renderer* _renderer = nullptr;
    TextureManager* _textureManager = nullptr;
    SDL_Rect _srcRect, _destRect;
};


#endif //VOXOL3_TILEPAINTER_H
