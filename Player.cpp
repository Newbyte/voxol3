//
// Created by neboula on 2019-04-27.
//

#include "Player.h"

Player::Player(SDL_Renderer *renderer, SDL_Texture *texture, MapManager* map, float x, float y) : PhysicalEntity(renderer, texture, map, x, y) {
    _height = 2;
}

void Player::update() {
    PhysicalEntity::update();
}

void Player::jump() {
    if (_onGround) {
        _resultingForce = -0.2;
    }
}
