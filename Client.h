//
// Created by neboula on 10/03/19.
//

#ifndef VOXOL3_CLIENT_H
#define VOXOL3_CLIENT_H


#include "MapManager.h"
#include "TextureManager.h"
#include "InputManager.h"
#include "TilePainter.h"
#include "Player.h"
#include "Camera.h"
#include "EntityManager.h"
#include <SDL2/SDL_render.h>

enum class GameState { PLAYING, EXIT };

class Client {
public:
    /// Constructs a new Client
    explicit Client(std::string worldName);
    Client(const std::string& host, u_int16_t port);
    /// Destructs the Client and frees memory
    ~Client();
private:
    /// Check for events (such as keyboard input)
    static void handleEvents(InputManager* inputManager, GameState* gameState);
    /// Update things
    void update();
    /// Draw things to screen
    void draw();

    TilePainter _painter;
    MapManager _map;
    TextureManager _textureManager;
    InputManager _inputManager;
    EntityManager _entityManager;
    Camera _camera;
    Player* _player;
    SDL_Window* _window = nullptr;
    SDL_Renderer* _renderer = nullptr;
    GameState _gameState = GameState::PLAYING;
};


#endif //VOXOL3_CLIENT_H
